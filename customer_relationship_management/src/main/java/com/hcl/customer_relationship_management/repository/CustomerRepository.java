package com.hcl.customer_relationship_management.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.customer_relationship_management.entity.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

}
