package com.hcl.customer_relationship_management.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.hcl.customer_relationship_management.DTO.CustomerDTO;
import com.hcl.customer_relationship_management.entity.Customer;
import com.hcl.customer_relationship_management.service.CustomerServiceImp;
import com.hcl.customer_relationship_management.service.ICustomerService;

@Controller
public class CustomerController {

	@Autowired
	ICustomerService service;
	
	@GetMapping("/customer")
	public String listCustomers(Model model) {
		model.addAttribute("customer", service.getAllCustomers());
		return "customer"; 
	}
	
	@GetMapping("/customer/new")
	public String createCustomer(Model model) {
		Customer customer=new Customer();
		model.addAttribute("customer",customer);
		return "create_customer";
	}
	
	@PostMapping("/customer")
	public String saveCustomer(@ModelAttribute("customer") CustomerDTO customerDTO ) {
		
		service.saveCustomer(customerDTO);
		return "redirect:/customer";
	}
	
    @GetMapping("/customer/edit/{id}")
    public String editCustomerForm(@PathVariable long id, Model model) {

   
        model.addAttribute("customer", service.getCustomerById(id));
        return "edit_customer";
    }
    
    @PostMapping("/customer/{id}")
    public String updateCustomer(@PathVariable Long id,@ModelAttribute("customer") Customer customer,Model model) {
    	
    	Customer existingCustomer = service.getCustomerById(id);
		
		existingCustomer.setFirstName(customer.getFirstName());
		existingCustomer.setLastName(customer.getLastName());
		existingCustomer.setEmail(customer.getEmail());
		
		// save updated customer object
		service.updateCustomer(existingCustomer);
		return "redirect:/customer";
    }

    @GetMapping("/customer/{id}")
    public String deleteCustomer(@PathVariable long id) {

        // call delete customer method 
        service.deleteCustomerById(id);
        return "redirect:/customer";
    }
	
	
	
}
