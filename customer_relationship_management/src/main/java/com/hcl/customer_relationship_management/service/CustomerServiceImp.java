package com.hcl.customer_relationship_management.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.customer_relationship_management.DTO.CustomerDTO;
import com.hcl.customer_relationship_management.entity.Customer;
import com.hcl.customer_relationship_management.repository.CustomerRepository;

@Service
public class CustomerServiceImp implements ICustomerService {
	
	@Autowired
	CustomerRepository repo;

	@Override
	public List<Customer> getAllCustomers() {
		
		
		return repo.findAll();
	}

	@Override
	public Customer saveCustomer(CustomerDTO customerDTO) {

		
		Customer customer=new Customer();
		customer.setFirstName(customerDTO.getFirstName());
		customer.setLastName(customerDTO.getLastName());
	     return repo.save(customer);
		
	}

	@Override
	public Customer getCustomerById(long id) {
		
		return repo.findById(id).orElse(null);
	}

	@Override
	public Customer updateCustomer(Customer customer) {

		return repo.save(customer);
	}
	
	@Override
	public void deleteCustomerById(long id) {
		this.repo.deleteById(id);
		
		

	}
}
