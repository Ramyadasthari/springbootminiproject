package com.hcl.customer_relationship_management.service;

import java.util.List;

import com.hcl.customer_relationship_management.DTO.CustomerDTO;
import com.hcl.customer_relationship_management.entity.Customer;

public interface ICustomerService {

	public List<Customer> getAllCustomers();
	public Customer saveCustomer(CustomerDTO customerDTO);
	public Customer getCustomerById(long id);
	public Customer updateCustomer(Customer customer);
	public void deleteCustomerById(long id);
	
}
